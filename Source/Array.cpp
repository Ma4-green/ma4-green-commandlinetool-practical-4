//
//  Array.cpp
//  CommandLineTool
//
//  Created by Matthew Green on 09/10/2014.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#include "Array.h"

Array::Array()
{
// create the array and set the size of it
    arrayPointer=nullptr;
    arraySize=0;
}

Array::~Array()
{
//test to see if the array is null if it is not then delete the array
    if (arrayPointer != nullptr)
        delete[] arrayPointer;
}

void Array::add (float itemValue)

{
// create a new array
    float* newArray = nullptr;
//create a new float that is the size of one plus arraySize and then make the new array that size
    newArray = new float[arraySize+1];
//cycly throught the old array and copy the data into the corresponding points in the new array
    for (int i=0; i < arraySize ; i++)
    {
        newArray[i] = arrayPointer[i];
    }
//assign the latest point as the new item value
    newArray[arraySize] = itemValue;
//if the old array isnt null then delete it
    if (arrayPointer != nullptr)
        delete[] arrayPointer;
//rename the new array with the original arrays name
    arrayPointer = newArray;
//update the record of the size of the array
    arraySize++;
}

float Array::get(int index)
{
//returns the data in the index point of the array
    return arrayPointer[index];
}

int Array::size()
{
//return the size of the array
    return arraySize;
}